window.onload = function() {
  document.getElementsByTagName("body")[0].className = ""
  setTimeout(function() {
    document.getElementById("t1").style.opacity = 1
  }, 750)
  setTimeout(function() {
    document.getElementById("t2").style.opacity = 1
  }, 1750)
  setTimeout(function() {
    document.getElementById("home-content").style.opacity = 1
  }, 2250)

  let links = document.getElementsByClassName("links")[0].children
  let timer = 2750
  for (let i = 0; i < links.length; i++) {
    setTimeout(function() {
      links[i].style.opacity = 1
      links[i].style.marginRight = ""
    }, timer)
    timer += 400
  }
}

var isHome = true;

function showSense() {
  if (isHome) {
    document.getElementById("question").style.opacity = 0
    document.getElementById("sense").style.display = "block"
    document.getElementById("home-content").style.opacity = 0
    setTimeout(function() { 
      document.getElementById("t1").style.top = "24%";
    }, 300)
    setTimeout(function() { document.getElementById("sense").style.opacity = 1 }, 800)
  }

  isHome = false
}